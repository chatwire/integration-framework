<?php

namespace Chatdart\IntegrationFramework;

use \Psr\Http\Message\ServerRequestInterface as Request;

abstract class AbstractIntegration
{

	/**
	 * The parent model
	 *
	 * @var \Chatdart\Integration
	 */
	public $integration;

	/**
	 * The integration type model
	 *
	 * @var \Chatdart\IntegrationType
	 */
	public $integrationType;

	/**
	 * Before saving a new integration, we help our users by
	 * generating a name for it. Return the initial name you
	 * want for this particular integration.
	 *
	 * This should be something descriptive, such as the name of
	 * your username on the chat service or mailbox on backend
	 *
	 * If you can't get a name, return an empty string.
	 *
	 * @return string
	 */
	abstract public function getInitialName();

	/**
	 * Get a Chatdart Customer object from an inbound request
	 * (likely a webhook)
	 *
	 * @param array $body
	 *
	 * @return \Chatdart\Customer
	 */
	abstract public function getCustomerFromInboundRequest( array $body );

	/**
	 * Get a message from an inbound request (likely a webhook)
	 *
	 * @param array $body
	 *
	 * @return string
	 */
	abstract public function getMessageFromInboundRequest( array $body );

	/**
	 * Get a template for creating a new connection
	 *
	 * @return string
	 */
	abstract public function getConnectionTemplate();

	/**
	 * Get the value to be saved as the integration's unique token
	 *
	 * @return string
	 */
	abstract public function getIntegrationUniqueToken();

	/**
	 * Get the unique token for the connection
	 *
	 * @return string
	 */
	abstract public function getConnectionUniqueToken();

	/**
	 * Get the parsed auth data for the connection
	 *
	 * @param array $data
	 *
	 * @return array
	 */
	abstract public function getConnectionAuthData( $data = [] );

	/**
	 * Set up any webhooks or event listeners for the connection
	 */
	abstract public function setupConnection();

	/**
	 * Save the integration
	 *
	 * @param \Chatdart\Integration $integration
	 */
	public function setIntegration( \Chatdart\Integration $integration )
	{
		$this->integration = $integration;

		$this->setAuthProperties();
	}

	/**
	 * Set the channel
	 *
	 * @param \Chatdart\Channel $channel
	 */
	public function setChannel( \Chatdart\Channel $channel )
	{
		$this->channel = $channel;
	}

	/**
	 * Set the backend
	 *
	 * @param \Chatdart\Backend $backend
	 */
	public function setBackend( \Chatdart\Backend $backend )
	{
		$this->backend = $backend;
	}

	/**
	 * Set the connection
	 *
	 * @param \Chatdart\Connection $connection
	 */
	public function setConnection( \Chatdart\Connection $connection )
	{
		$this->connection = $connection;
	}

	/**
	 * We shouldn't process every request from backends
	 *
	 * @param \Request $request PSR7-compatible request
	 *
	 * @return bool True if we should process, false to not process
	 */
	public function shouldProcessRequest( \Request $request )
	{
		return true;
	}

	/**
	 * Get a prefixed field name, with the integration type and slug
	 *
	 * @param string $name
	 *
	 * @return string
	 */
	public function getPrefixedFieldName( $name )
	{
		return "{$this->integration->type->type}_{$this->integration->type->slug}_{$name}";
	}

}
