<?php

namespace Chatdart\IntegrationFramework\Interfaces\Types;

interface ChannelInterface
{

	/**
	 * Send a message to the Customer via the channel
	 *
	 * @param \Chatdart\Customer $customer Who this message is going to
	 * @param string             $message  The contents of the message
	 *
	 * @return bool
	 */
	function sendMessage( \Chatdart\Customer $customer, $message );

}
