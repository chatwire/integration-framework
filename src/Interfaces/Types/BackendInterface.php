<?php

namespace Chatdart\IntegrationFramework\Interfaces\Types;

interface BackendInterface
{

	/**
	 * Create a new support thread
	 *
	 * @param \Chatdart\Customer $customer
	 * @param string             $message
	 *
	 * @return bool|int False if it fails, thread ID int if true
	 */
	public function newThread( \Chatdart\Customer $customer, $message );

	/**
	 * Reply to an existing support thread
	 *
	 * @param \Chatdart\Customer $customer
	 * @param string             $message
	 *
	 * @return bool
	 */
	public function newThreadReply( \Chatdart\Customer $customer, $message );

}
