<?php

namespace Chatdart\IntegrationFramework\Interfaces;

interface IntegrationFieldsInterface
{

	/**
	 * Form fields we're going to display publicly
	 *
	 * @return array
	 */
	public function getIntegrationFields();

	/**
	 * Validation rules for integration fields
	 *
	 * @return array
	 */
	public function getIntegrationFieldsValidationRules();

	/**
	 * Custom validation rules to be registered
	 *
	 * @return array
	 */
	public function getCustomValidationRules();

}
