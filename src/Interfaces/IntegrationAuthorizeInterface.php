<?php

namespace Chatdart\IntegrationFramework\Interfaces;

use \Psr\Http\Message\ServerRequestInterface as Request;

interface IntegrationAuthorizeInterface
{

	/**
	 * Process the authorisation request
	 *
	 * @param Request $request
	 *
	 * @return \Chatdart\Integration
	 */
	public function authorize( Request $request );

	public function deauthorize();

}
