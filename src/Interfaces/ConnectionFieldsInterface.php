<?php

namespace Chatdart\IntegrationFramework\Interfaces;

interface ConnectionFieldsInterface
{

	/**
	 * Form fields we're going to display publicly
	 *
	 * @return array
	 */
	public function getConnectionFields();

	/**
	 * Validation rules for the public fields
	 *
	 * @return array
	 */
	public function getConnectionFieldsValidationRules();

	/**
	 * Custom validation rules to be registered
	 *
	 * @return array
	 */
	public function getCustomValidationRules();

}
