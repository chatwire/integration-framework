<?php

namespace Chatdart\IntegrationFramework\Exceptions;

use Exception;

class RemoteError extends Exception {}
