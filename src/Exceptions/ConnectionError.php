<?php

namespace Chatdart\IntegrationFramework\Exceptions;

use Exception;

class ConnectionError extends Exception {}
