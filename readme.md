# Chatdart Integration Framework

An abstract class and interfaces for defining Chatdart-compatible backend and channel integrations.

## Terminology

+ An **Integration** is an individual account or service that's connected to Chatdart. This could be a 3rd party service, such as SMS, Groove, etc., or it can be one of our custom code options (e.g. a POST to a webhook, or our own webhook where users can point their application).
+ A **Connection** is a pair of previously-defined Integrations—a Channel and a Backend—plus any additional metadata (such as the "Mailbox ID" for Help Scout or Groove, or the inbound/outbound number for Twilio) that allow the two to communicate seamlessly with each other.
+ **Customers** are the end-users who are interacting with the companies who have signed up for Chatdart. They interact with directly with Channels.
+ A **Channel** integration is how Customers interface with the company. SMS, WhatsApp, and Telegram would all be examples of channels.
+ A **Backend** integration is the opposite side, where messages from Customers are read and responded to. Examples include Help Scout, Groove, and FrontApp.

## In the database…

…Integrations and Connections are stored in the database as follows:

| Table                |  Description |
|----------------------|--------------|
| `integration_types`  |  Stores the specific types of Integrations that are available to Chatdart users |
| `integrations`       |  Stores Integrations that Chatdart users have already connected, including the auth data/tokens |
| `channels`           |  Stores the defined Channels that Chatdart automatically creates during the New Connection process, including any specific data about that channel (such as an SMS number for Twilio) |
| `backends`           |  Stores defined Backends that Chatdart automatically creates during the New Connection process, including specific data about that backend (such as a Mailbox ID for Help Scout/Groove) |
| `connections`        |  A pivot table (sort of) that defines the backend/channel to be linked, plus (in the future) any additional metadata about the connection. |

## A typical flow

All Integrations follow _generally similar_ flows. Generally, that flow looks like this:

0. A user decides to add a new Integration to their Chatdart account
0. The user chooses which Integration to connect, via `/integration/create`
0. If the Integration is of the Authorize type, the user authenticates via a standard OAuth flow (see: Groove)
0. If the Integration is of the Fields type, the user enters information required to authorise the account, such as an API key

To link two Integrations and create a Connection—the heart of Chatdart—the user must:

0. From the list of Integrations the user has already connected, determine which Integration will serve as the Connection Channel and which will serve as the Connection Backend, via `/connection/create`
0. Enter any settings to configure the connection to the Channel (e.g. the SMS number in Twilio, or the mailbox ID in Groove/Help Scout)
0. Enter any additional settings about the connection (e.g. provide a custom name, custom callbacks or webhooks, etc.)

## Using the Integration Framework

Each integration must extend the AbstractIntegration abstract class, and implement a few interfaces, depending on the nature of the specific integration.

### Note:

The backend/channel paradigm may not be the best way to understand these different services, as not all services may fit neatly into this model. For example, Slack could conceivably serve as both a channel _and_ a backend, depending on how it's configured.

To accommodate these situations, we may want to revise the integration framework, OR simply write separate backend/channel integrations for these apps.

## Abstract class

All integrations must extend the AbstractIntegration class.

### `Chatdart\IntegrationFramework\AbstractIntegration`

Should implement the following:

+ `getInitialName`
+ `getCustomerFromInboundRequest`
+ `getMessageFromInboundRequest`
+ `getConnectionTemplate`
+ `getIntegrationUniqueToken`
+ `getConnectionUniqueToken`
+ `getConnectionAuthData`
+ `setupConnection`

The following methods will be available to the Integration:

+ `setIntegration`
+ `setChannel`
+ `setBackend`
+ `setConnection`
+ `shouldProcessRequest`
+ `getPrefixedFieldName`

## Interfaces

The following interfaces are available:

### `Chatdart\IntegrationFramework\Interfaces\Types\BackendInterface`

Defines an integration as a Backend.

Must implement the following methods:

+ `newThread` - Creates a new message thread in the Backend software
+ `newThreadReply` - Creates a new reply in an existing message thread in the backend software

### `Chatdart\IntegrationFramework\Interfaces\Types\ChannelInterface`

Defines an integration as a Channel.

Must implement the following methods:

+ `sendMessage` - Send a message to the user

### `Chatdart\IntegrationFramework\Interfaces\ConnectionFieldsInterface`

Defines the specific fields needed to complete a connection.

+ `getConnectionFields` - Defines the fields a user must fill to create the Connection
+ `getConnectionFieldsValidationRules` - Defines the rules to use to validate the fields
+ `getCustomValidationRules` - Defines any custom validation rules (that may or may not be used)

### `Chatdart\IntegrationFramework\Interfaces\IntegrationAuthorizeInterface`

Defines an Integration as one authorised through an OAuth (or similar) flow. Must implement `IntegrationAuthorizeInterface` OR `IntegrationFieldsInterface`, but **NOT BOTH**.

+ `authorize` - Parse the request after returned from the OAuth (or similar) login, and collect an access token
+ `deauthorize` - Handle requests to deauthorize the 3rd party app

### `Chatdart\IntegrationFramework\Interfaces\IntegrationFieldsInterface`

Defines the fields needed to connect an Integration.

+ `getIntegrationFields` - Defines the fields a user must fill to connect the Integration
+ `getIntegrationFieldsValidationRules` - Defines the rules to use to validate the fields
+ `getCustomValidationRules` - Defines any custom validation rules (that may or may not be used)


## License

(c) 2016 Van Patten Media Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
